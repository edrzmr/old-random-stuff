#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

bool
is_odd(unsigned n)
{
	return (n % 2);
}

unsigned
xnext(unsigned n)
{
	return (is_odd(n)) ? 3*n+1 : n/2;
}

unsigned
count_seq(unsigned n)
{
	unsigned acm = 1;

	while (n > 1) {
		n = xnext(n);
		acm++;
	}

	return acm;
}

bool
max_seq(unsigned ini, unsigned *gc, unsigned *gini)
{
	unsigned aux;

	if (NULL == gc || NULL == gini)
		return false;

	*gc = 0;
	*gini = 0;

	while (ini > 1) {

		aux = count_seq(ini);
		if (aux > *gc) {
			*gc = aux;
			*gini = ini;
		}
		ini--;
	}

	return true;
}

int
main(int argc, char *argv[])
{
	unsigned great_count;
	unsigned great_ini;
	unsigned seq_len;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <seq_len>\nexample: %s 1000000\n", argv[0], argv[0]);
		return -1;
	}

	seq_len = atoi(argv[1]);
	printf("-> sequencias a serem testadas: %u\n", seq_len);
	max_seq(seq_len, &great_count, &great_ini);
	printf("-> maior numero de elementos de uma sequencia: %u\n", great_count);
	printf("-> elemento que gera essa maior sequencia: %u\n", great_ini);

	return 0;
}
