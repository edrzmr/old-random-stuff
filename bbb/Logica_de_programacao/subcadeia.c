#include <stdio.h>

#define MAX_ARRAY_SIZE 1000
#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

typedef struct {
	int sum;
	unsigned ini;
	unsigned end;
} sum_t;

#ifdef DEBUG
static void
sum_print(sum_t *sum)
{
	printf("sum: %i, ini: %u, end: %u\n", sum->sum, sum->ini, sum->end);
}
#endif

static void
array_print(int *array, unsigned ini, unsigned len)
{
	unsigned i;
	unsigned end = len-1;

	if (NULL == array)
		return;

	for (i = ini; i < end; i++) {

		printf("%i ", array[i]);
	}
	printf("%i\n", array[i]);
}

static void
array_max_sum(int *array, unsigned ini, unsigned len, sum_t *max)
{
	int max_sum = array[ini];
	int sum = 0;
	unsigned end = ini;

	if (NULL == array || NULL == max)
		return;

	for (unsigned i = ini; i < len; i++) {

		sum += array[i];
		if (sum >  max_sum) {
			max_sum = sum;
			end = i;
		}
#ifdef DEBUG
		printf("i: %u, sum: %i, max: %i, end: %u\n", i, sum, max_sum, end);
#endif
	}
	max->sum = max_sum;
	max->ini = ini;
	max->end = end;
}

static void
array_max_sum_sub_arrays(int *array, unsigned len, sum_t *max)
{
	sum_t tmp[1];

	if (NULL == max || NULL == array)
		return;

	tmp->sum = array[0];
	tmp->ini = 0;
	tmp->end = 0;
	max->sum = array[0];
	max->ini = 0;
	max->end = 0;
	for (unsigned i = 0; i < len; i++) {
		array_max_sum(array, i, len, tmp);
		if (tmp->sum > max->sum) {
			max->sum = tmp->sum;
			max->ini = tmp->ini;
			max->end = tmp->end;
		}
	}
#ifdef DEBUG
	sum_print(max);
#endif
}

int
main(void)
{
	int array[MAX_ARRAY_SIZE];
	unsigned len = 0;
	sum_t max[1];

	while(0 == feof(stdin)) {

		if (len >= MAX_ARRAY_SIZE) {
			fprintf(stderr, "no more memory\n");
			return -1;
		}
		fscanf(stdin, "%i\n", &array[len++]);
	}

	array_max_sum_sub_arrays(array, len, max);

	printf("-> array: ");
	array_print(array, 0, len);
	printf("-> max sum: %i\n", max->sum);
	printf("-> ini: %i\n", max->ini);
	printf("-> end: %i\n", max->end);

	return 0;
}
