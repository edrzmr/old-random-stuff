$(document).ready(function()
{

  var API_URL = "{{API_URL}}" + '/statistics';
  $.getJSON(API_URL, function(d)
  {
    var candidates = d.candidates;
    var total = d.total;
    var data = [];

    $.each(d.tree, (function(year, months) {
      $.each(months, (function(month, days) {
        $.each(days, (function(day, hours) {
          $.each(hours, (function(hour, votes) {

              data.push({
                ts: year + '-' + month + '-' + day + ' ' + hour + ':00',
                votes: votes
              });

          }));
        }));
      }));
    }));

    $('#c1 .cid').text(candidates[0].id);
    $('#c2 .cid').text(candidates[1].id);
    $('#c1 .name').text(candidates[0].name);
    $('#c2 .name').text(candidates[1].name);
    $('#c1 .votes').text(candidates[0].votes);
    $('#c2 .votes').text(candidates[1].votes);
    $('.total b').text(total);

    chart = Morris.Line({
      element: 'line-chart',
      data: data,
      xkey: 'ts',
      ykeys: ['votes'],
      labels: ['Votos']
    });
  }).fail(function(jq, st, e) {
    alert('Problemas em contatar o servidor: ' + API_URL);
  });
});
