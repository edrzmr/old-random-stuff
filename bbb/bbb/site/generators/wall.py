from generators import config


def gen():
    ret = {
        'wall_js': 'wall.js',
        'recaptcha_js': 'https://www.google.com/recaptcha/api.js'
    }
    ret.update(config.gen())

    return ret
