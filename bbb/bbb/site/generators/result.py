from generators import config


def gen():
    ret = {
        'result_js': 'result.js'
    }
    ret.update(config.gen())

    return ret
