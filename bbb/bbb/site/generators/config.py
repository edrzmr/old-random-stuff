def gen():
    return {
        'title': 'Paredão!',
        'API_URL': 'http://127.0.0.1:8080',
        'IMG_DIR': '/img',
        'css': 'css/bbb.css',
        'jquery_js': 'js/jquery-2.1.3.js',
        'bootstrap_js': 'bootstrap-3.3.4-dist/js/bootstrap.js',
        'bootstrap_css': 'bootstrap-3.3.4-dist/css/bootstrap.css',
        'raphael_js': 'js/raphael-2.1.2.js',
        'morris_js': 'js/morris-0.5.1.js'
    }
