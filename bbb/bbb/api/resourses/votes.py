from flask.ext import restful
from flask_restful import reqparse
from flask_restful import fields
from flask_restful import marshal_with
from flask_restful import inputs
from flask_restful import abort
from flask import jsonify
from flask import request
from api import mongo
from api import redis
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('id', type=int, required=True,
                    help='id is needed', location='json')
parser.add_argument('name', type=str, location='json')
parser.add_argument('grecaptcha', type=str, required=True,
                    help='grecaptcha is a needed field', location='json')

statistics_fields = {
    '1': fields.Integer,
    '2': fields.Integer,
}


def validate_recaptcha(grecaptcha, client_ip):

    from requests import post, codes
    r = post('https://www.google.com/recaptcha/api/siteverify', data={
             'secret': '6LcWjAYTAAAAAFk06nriova2mIK9IXslyk3dHoeU',
             'response': grecaptcha,
             'remoteip': client_ip})

    if r.status_code is not codes.ok:
        return False

    return r.json()['success']


class Votes(restful.Resource):

    @marshal_with(statistics_fields)
    def post(self):
        args = parser.parse_args()
        try:
            arg_cid = inputs.int_range(1, 2, args['id'])
            arg_grecaptcha = args['grecaptcha']
        except ValueError as e:
            abort(400, message=str(e))

        if not validate_recaptcha(arg_grecaptcha, request.remote_addr):
            abort(401, message='invalid captcha')

        now = datetime.utcnow()

        mongo.db.votes.insert(
            {
                'candidate_id': arg_cid,
                'ts': now
            })

        cid = 'candidate:' + str(arg_cid)
        hour = 'votes_by_hour:' + str(now.year) + ':' + str(
            now.month) + ':' + str(now.day) + ':' + str(now.hour)

        p = redis.pipeline()
        p.incr(cid, 1)
        p.incr(hour, 1)
        p.get('candidate:1')
        p.get('candidate:2')
        (_, _, v1, v2) = p.execute()

        return {
            '1': 0 if v1 is None else int(v1),
            '2': 0 if v2 is None else int(v2)
        }

    def get(self):
        p = redis.pipeline()
        p.get('candidate:1')
        p.get('candidate:2')
        (v1, v2) = p.execute()
        return jsonify({
            'votes': [
                {
                    'id': 1,
                    'votes': 0 if v1 is None else int(v1),
                    'name': 'Vinícius de Moraes'
                }, {
                    'id': 2,
                    'votes': 0 if v2 is None else int(v2),
                    'name': 'Clarice Lispector'
                }
            ]})

    def options(self):
        return ''
