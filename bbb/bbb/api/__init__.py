from flask import Flask
from flask.ext import restful
from flask.ext.cors import CORS
from flask.ext.pymongo import PyMongo
from flask.ext.redis import Redis


app = Flask(__name__)
api = restful.Api(app)
cors = CORS(app)
mongo = PyMongo(app)
redis = Redis(app)

from api.resourses.candidates import Candidates
from api.resourses.votes import Votes
from api.resourses.statistics import Statistics


api.add_resource(Candidates, '/candidates')
api.add_resource(Votes, '/votes')
api.add_resource(Statistics, '/statistics')

if not app.debug:
    import logging
    handler = logging.FileHandler('/tmp/flask.log')
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
