#!/bin/bash

uid=$(id -u)
if [ ${uid} -ne 0 ]
then
	echo
	echo " -> you must be root!"
	echo
	exit -1
fi

SCRIPT_DIR=$(cd $(dirname $0) && pwd)

echo
echo " -> I'm $(basename $0)"
echo

cd ${SCRIPT_DIR}
for script in *-root-install.sh; do
	echo
	echo " -> running ${script}"
	echo
	./${script}
done
