#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
ENV_PATH=${ENV_PATH:-$(cd ${SCRIPT_DIR}/../env && pwd)}
PACKAGES="nginx"

echo ENV_PATH: ${ENV_PATH}
echo SCRIPT_DIR: ${SCRIPT_DIR}


# if nginx is installed do nothing
for pkg in ${PACKAGES}; do
	pkg_name=$(cut -d"=" -f1 <<< ${pkg})
	pkg_status=$(dpkg -l ${pkg_name} 2> /dev/null | tail -n1 | cut -d" " -f1)
	if [ "ii" != "${pkg_status}" -a "hi" != "${pkg_status}" ]; then
		pkg_list+="${pkg} "
	fi
done
if [ -z "${pkg_list}" ]; then
	echo
	echo " -> nginx already installed :)"
	echo
	usermod -a -G www-data nginx
	exit 0
fi


ETC_DIR="/etc/nginx"
STATIC_FILES_DIR=$(cd ${SCRIPT_DIR}/../site/output && pwd)
UWSGI_SOCKETFILE="/run/uwsgi/app/bbb/socket"

echo ETC_DIR: ${ETC_DIR}
echo STATIC_FILES_DIR: ${STATIC_FILES_DIR}
echo UWSGI_SOCKETFILE: ${UWSGI_SOCKETFILE}

KEY="7BD9BF62"
KEY_URL="http://nginx.org/keys/nginx_signing.key"
FINGER="573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62"

read_finger()
{
	echo $(apt-key finger | grep -A 1 $KEY | tail -n1 | cut -d"=" -f2 | tr -d " ")
}

# setup sources.list
dist=$(lsb_release -si)
filename="/etc/apt/sources.list.d/nginx.list"
case ${dist} in
	"Debian")
		echo "deb http://nginx.org/packages/debian/ jessie nginx" | tee ${filename}
		;;
	"Ubuntu")
		echo "deb http://nginx.org/packages/ubuntu/ "$(lsb_release -sc)" nginx" | tee ${filename}
		;;
	*)
		echo
		echo "* oops, I don't know how install nginx in your system"
		echo
		exit -1
esac

# install key to apt
finger=$(read_finger)
if [ -z $finger ]; then
	wget -qO- ${KEY_URL} | apt-key add -
fi

# validate fingerprints 
finger=$(read_finger)
if [ "${FINGER}" != "${finger}" ]; then
	echo
	echo " -> oops, crazy fingerprints"
	echo
	exit -2
fi

apt-get -q update
apt-get -q -y install ${pkg_list}

usermod -a -G www-data nginx

cp ${SCRIPT_DIR}/nginx.tpl.conf ${ETC_DIR}/nginx.conf
sed -i "s|###STATIC_FILES_DIR###|${STATIC_FILES_DIR}|" ${ETC_DIR}/nginx.conf
sed -i "s|###UWSGI_SOCKETFILE###|${UWSGI_SOCKETFILE}|" ${ETC_DIR}/nginx.conf
/etc/init.d/nginx restart

echo
echo " -> nginx is installed now \\o/"
echo
