#!/bin/bash

uid=$(id -u)
if [ ${uid} -ne 0 ]
then
	echo
	echo " -> you must be root!"
	echo
	exit -1
fi

echo
echo " -> I'm $(basename $0)"
echo

SCRIPT_DIR=$(cd $(dirname $0) && pwd)

cd ${SCRIPT_DIR}
for script in *-root-config.sh; do
	echo
	echo " -> running ${script}"
	echo
	./${script}
done
