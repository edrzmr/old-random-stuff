#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
ENV_PATH=${ENV_PATH:-$(cd ${SCRIPT_DIR}/../env && pwd)}
PACKAGES="apache2-utils"

echo ENV_PATH: ${ENV_PATH}
echo SCRIPT_DIR: ${SCRIPT_DIR}

# if apache2-utils is installed do nothing
for pkg in ${PACKAGES}; do
	pkg_name=$(cut -d"=" -f1 <<< ${pkg})
	pkg_status=$(dpkg -l ${pkg_name} 2> /dev/null | tail -n1 | cut -d" " -f1)
	if [ "ii" != "${pkg_status}" -a "hi" != "${pkg_status}" ]; then
		pkg_list+="${pkg} "
	fi
done
if [ -z "${pkg_list}" ]; then
	echo
	echo " -> apache2-utils already installed :)"
	echo
	exit 0
fi

apt-get -q -y install ${PACKAGES}

echo
echo " -> apache2-utils is installed now \\o/"
echo
