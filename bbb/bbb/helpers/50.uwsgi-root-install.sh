#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
PROJECT_DIR=$(cd ${SCRIPT_DIR}/.. && pwd)

echo SCRIPT_DIR: ${SCRIPT_DIR}

PACKAGES="uwsgi uwsgi-plugin-python3"

# if uwsgi is installed do nothing
for pkg in ${PACKAGES}; do
	pkg_name=$(cut -d"=" -f1 <<< ${pkg})
	pkg_status=$(dpkg -l ${pkg_name} 2> /dev/null | tail -n1 | cut -d" " -f1)
	if [ "ii" != "${pkg_status}" -a "hi" != "${pkg_status}" ]; then
		pkg_list+="${pkg} "
	fi
done
if [ -z "${pkg_list}" ]; then
	echo
	echo " -> uwsgi already installed :)"
	echo
	exit 0
fi

apt-get -q update
apt-get -q -y install ${PACKAGES}

echo
echo " -> uwsgi is installed now \\o/"
echo
