#/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
ENV_PATH=${ENV_PATH:-"${SCRIPT_DIR}/../env"}
rm -rf ${ENV_PATH}
mkdir -p ${ENV_PATH}
ENV_PATH=$(cd ${ENV_PATH} && pwd)

echo SCRIPT_DIR: ${SCRIPT_DIR}
echo ENV_PATH: ${ENV_PATH}

PIP=${PIP:-pip3}
ACTIVATE_CMD="${ENV_PATH}/bin/activate"

do_source()
{
	echo ""
	echo "-> use: source ${ACTIVATE_CMD} to activate the environment"
	echo ""
}

${PIP} install virtualenv --upgrade --user
${HOME}/.local/bin/virtualenv ${ENV_PATH}
do_source

echo "source ${ACTIVATE_CMD}"
source ${ACTIVATE_CMD}
${PIP} install -r ${SCRIPT_DIR}/requirements.txt
