#/bin/bash

PIP=${PIP:-pip3}
if ${PIP} -Vq > /dev/null 2>&1
then
	exit 0
fi

dist=$(lsb_release -si)
case ${dist} in
	"Debian" | "Ubuntu")
		apt-get -q -y install python3-pip
		;;
	*)
		echo
		echo " -> ${PIP} must be installed and in your PATH"
		echo
		exit -1
	;;
esac
