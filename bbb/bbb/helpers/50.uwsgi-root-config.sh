#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd)
ENV_PATH=$(cd ${SCRIPT_DIR}/../env && pwd)
PROJECT_DIR=$(cd ${SCRIPT_DIR}/.. && pwd)

echo ENV_PATH: ${ENV_PATH}
echo SCRIPT_DIR: ${SCRIPT_DIR}

ETC_DIR="/etc/uwsgi"
INIFILE="${ETC_DIR}/apps-available/bbb.ini"

cp ${SCRIPT_DIR}/uwsgi.tpl.ini ${INIFILE}
sed -i "s|###VENV###|${ENV_PATH}|" ${INIFILE}
sed -i "s|###CHDIR###|${PROJECT_DIR}|" ${INIFILE}
cd ${ETC_DIR}/apps-enabled
ln -s ${INIFILE} .

/etc/init.d/uwsgi restart

echo
echo " -> uwsgi is configured now \\o/"
echo
