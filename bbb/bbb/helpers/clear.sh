#!/bin/bash

uid=$(id -u)
if [ ${uid} -ne 0 ]
then
	echo
	echo " -> you must be root!"
	echo
	exit -1
fi

SERVICES="uwsgi nginx redis mongodb python3-pip apache2-utils"

echo SERVICES: ${SERVICES}

for s in ${SERVICES}; do
	pkgs_list+=$(dpkg -l | grep "${s}" | awk '{print $2}' | tr "\n" " ")
done
apt-get -q -y --force-yes --auto-remove remove --purge ${pkgs_list}

rm -rf /etc/uwsgi
rm -rf /var/run/uwsgi
rm -f /etc/apt/sources.list.d/*

for s in ${SERVICES}; do
	rm -rf /var/log/${s}*
	rm -rf /var/run/${s}*
done
