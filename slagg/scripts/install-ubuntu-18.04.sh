#!/bin/bash

VERSION="v0.2.0"

set -e

export LC_ALL=C.UTF-8
export LANG=C.UTF-8

echo
echo
echo " => install dependences"
echo
echo
sudo add-apt-repository --yes --update ppa:webupd8team/java
echo 'oracle-java8-installer shared/accepted-oracle-license-v1-1 select true' | sudo debconf-set-selections
sudo apt-get install -qy python3-pip virtualenv oracle-java8-set-default oracle-java8-installer
echo
echo
echo " => create environment"
echo
echo
virtualenv -p python3 slagg-env
source slagg-env/bin/activate
echo
echo
echo " => install slagg"
echo
echo
pip3 install git+https://github.com/drrzmr/slagg.git@${VERSION}
echo
echo
