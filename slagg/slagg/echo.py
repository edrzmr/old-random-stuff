from click import echo, secho


def title_single(text):
    raise NotImplementedError(
        'type="{}", value="{}"'.format(type(text), text))


def title_subtitle(text, subtext):
    text = '%s: ' % text
    space_line = '~' * (len(text) + len(subtext))

    secho(space_line, fg='magenta', bold=True)
    secho(text, fg='green', bold=True, nl=False)
    secho(subtext, fg='blue', bold=True)
    secho(space_line, fg='magenta', bold=True)


def title(text, subtext=None):
    if subtext is None:
        return title_single(text)

    return title_subtitle(text, subtext)


def item(value):
    if isinstance(value, str):
        echo(' -> %s' % value)
        return

    if isinstance(value, tuple):
        k, v = value
        config('* {}'.format(k), v)
        return

    raise NotImplementedError(
        'type="{}", value="{}"'.format(type(value), value))


def config(key, value):
    if isinstance(value, str) or \
            isinstance(value, bool) or \
            isinstance(value, int):
        secho('%s: ' % key, fg='green', bold=True, nl=False)
        echo(value)
        return

    if isinstance(value, tuple):
        secho('%s: ' % key, fg='green', bold=True)
        for i in value:
            item(i)
        return

    raise NotImplementedError(
        'type="{}", value="{}"'.format(type(value), value))


def configs(header=None, subheader=None, pairs=None):
    if header is not None:
        title(header, subheader)

    if pairs is None:
        return

    if isinstance(pairs, list) or isinstance(pairs, tuple):
        for k, v in pairs:
            config(k, v)
        return

    raise NotImplementedError(
        'type="{}", value="{}"'.format(type(value), value))
