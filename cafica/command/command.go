package command

import (
	"strings"

	"github.com/alecthomas/kingpin"
	"github.com/drrzmr/cafica/config"
	"github.com/drrzmr/cafica/logger"
)

// Callback is the function for Run
type Callback func(context *Context) error

type command struct {
	conf   config.Config
	log    logger.Logger
	cmd    string
	parsed string

	flags map[string]bool
	app   *kingpin.Application
	topic topicCommand
}

type topicCommand struct {
	clause *kingpin.CmdClause
	list   topicListCommand
}

type topicListCommand struct {
	clause *kingpin.CmdClause
}

func (cmd *command) load(args []string) *command {

	cmd.conf = config.Default()
	flags := make(map[string]*bool)

	/* >>> App >>> */
	kafkaVersion := cmd.app.Flag("kafka.version", "Kafka version").
		Envar("CAFICA_KAFKA_VERSION").
		Default(cmd.conf.Kafka.Version).
		String()

	kafkaBrokers := cmd.app.Flag(
		"kafka.brokers",
		"Comma separated list of kafka brokers (example1.org:9092,example2.org:9092)").
		Envar("CAFICA_KAFKA_BROKERS").
		Required().
		Short('b').
		String()

	logLevel := cmd.app.Flag(
		"log.level",
		"Set log level.").
		HintOptions(
			logger.InfoLevel.String(),
			logger.DebugLevel.String(),
			logger.ErrorLevel.String(),
			logger.WarnLevel.String()).
		Envar("CAFICA_LOGLEVEL").
		Short('l').
		Default(cmd.conf.LogLevel.String()).
		String()
	/* <<< App flags <<< */

	/* >>> App.Topic >>> */
	cmd.topic.clause = cmd.app.Command("topic", "Kafka topic stuff.").Default()
	{
		/* >>> Topic.List >>> */
		list := cmd.topic.clause.Command("list", "List kafka topics.").Default()
		flags["with.size"] = list.Flag(
			"with.size",
			"Estimate number os messages in topics").
			Default("false").
			Envar("CAFICA_KAFKA_TOPIC_LIST_WITH_SIZE").
			Short('s').
			Bool()
		cmd.topic.list.clause = list
		/* <<< Topic.List <<< */
	}

	cmd.parsed = strings.Join(
		strings.Split(
			kingpin.MustParse(
				cmd.app.Parse(args)), " "), ".")

	cmd.conf.LogLevel = logger.Level(*logLevel)
	cmd.conf.Kafka.Version = *kafkaVersion
	cmd.conf.Kafka.Brokers = strings.Split(*kafkaBrokers, ",")

	for name, ptr := range flags {
		cmd.flags[name] = *ptr
	}

	return cmd
}

func (cmd *command) configure() *command {
	logger.Configure(cmd.conf.LogLevel)
	cmd.log = logger.Get()
	return cmd
}

func (cmd *command) run(callback Callback) error {

	return callback(newContext(
		cmd.conf,
		cmd.parsed,
		cmd.log,
		cmd.flags))
}

// Run parse args and call the callback function with a context
func Run(args []string, callback Callback) error {
	c := &command{
		app:   kingpin.New("cafica", "A kafka helper."),
		flags: make(map[string]bool),
	}
	return c.load(args).configure().run(callback)
}
