package data

// Row represents a table row
type Row struct {
	Topic          string
	Partition      int32
	LatestOffset   int64
	EarliestOffset int64
}

// Rows sintax sugar for []Row
type Rows []Row
