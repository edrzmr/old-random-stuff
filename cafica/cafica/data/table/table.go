package table

import (
	"github.com/drrzmr/cafica/cafica/data"
	"github.com/pkg/errors"
)

// New create a new table with given topics
func New(topics []string) data.Table {

	table := make(data.Table)
	for _, topic := range topics {
		table[topic] = data.Rows{data.Row{Topic: topic}}
	}

	return table
}

// WithTopics sintax suggar for New
func WithTopics(topics []string, err error) (data.Table, error) {
	if err != nil {
		return nil, errors.Wrap(err, "could not create table with topics")
	}
	return New(topics), nil
}
