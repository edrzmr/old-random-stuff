package data

// Partition represent one kafka partition with its information
type Partition struct {
	ID             int32
	LatestOffset   int64
	EarliestOffset int64
	Length         int64
}

// Partitions sintax sugar for []Partition
type Partitions []Partition
