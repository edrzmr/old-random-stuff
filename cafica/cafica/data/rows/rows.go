package rows

import (
	"github.com/drrzmr/cafica/cafica/data"
	"github.com/pkg/errors"
)

// New create a new data.Rows with given topic ans partitions
func New(topic string, partitions []int32) data.Rows {

	size := len(partitions)
	rows := make(data.Rows, size, size)
	for i, partition := range partitions {
		rows[i] = data.Row{
			Topic:     topic,
			Partition: partition,
		}
	}

	return rows
}

// WithPartitions sintax sugar for New
func WithPartitions(topic string, partitions []int32, err error) (data.Rows, error) {
	if err != nil {
		return nil, errors.Wrap(err, "could not create rows with partitions")
	}
	return New(topic, partitions), nil
}

// WithOffsets create a new [1]data.Row with given topic, partition, newest and oldest offset
func WithOffsets(topic string, partition int32, newest, oldest int64, err error) (data.Rows, error) {
	if err != nil {
		return nil, errors.Wrap(err, "could not create row with partition and offsets")
	}

	return data.Rows{
		data.Row{
			Topic:          topic,
			Partition:      partition,
			LatestOffset:   newest,
			EarliestOffset: oldest,
		},
	}, nil
}
