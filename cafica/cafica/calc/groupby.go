package calc

import (
	"sort"

	"github.com/drrzmr/cafica/cafica/data"
)

// GroupByTopicResult object with result of topic group by operation
type GroupByTopicResult struct {
	Topic      string
	Length     int64
	Partitions data.Partitions
}

// GroupByTopic create a groupby topic for given table
func GroupByTopic(table data.Table) []GroupByTopicResult {

	result := make([]GroupByTopicResult, 0)

	for topic, rows := range table {
		size := len(rows)
		resultItem := GroupByTopicResult{
			Topic:      topic,
			Partitions: make(data.Partitions, size, size),
		}
		length := int64(0)
		for i, row := range rows {
			resultItem.Partitions[i] = data.Partition{
				ID:             row.Partition,
				Length:         row.LatestOffset - row.EarliestOffset,
				EarliestOffset: row.EarliestOffset,
				LatestOffset:   row.LatestOffset,
			}
			length += resultItem.Partitions[i].Length
		}

		sort.Slice(resultItem.Partitions, func(i, j int) bool {
			return resultItem.Partitions[i].ID < resultItem.Partitions[j].ID
		})
		resultItem.Length = length
		result = append(result, resultItem)
	}

	sort.Slice(result, func(i, j int) bool {
		if result[i].Length == result[j].Length {
			return result[i].Topic < result[j].Topic
		}
		return result[i].Length > result[j].Length
	})
	return result
}
