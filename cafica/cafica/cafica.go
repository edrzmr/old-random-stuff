package cafica

import (
	"sync"

	"github.com/drrzmr/cafica/cafica/data"
	"github.com/pkg/errors"
)

// Config for cafica
type Config struct {
	ID      string
	Brokers []string
	Version string
	Backend ConnectorType
}

// Cafica struct
type Cafica struct {
	connector Connector
}

// SyncHandlerFunc callback type for sync handlers
type SyncHandlerFunc func(conn Connection, in data.Table) (data.Table, error)

// AsyncHandlerFunc callback type for async handlers
type AsyncHandlerFunc func(conn Connection, topicName string, partitionID int32) (data.Rows, error)

// New creates a cafica object
func New(config Config, connectorType ConnectorType) (*Cafica, error) {

	c, err := NewConnector(config, connectorType)
	if err != nil {
		return nil, errors.Wrap(err, "could not create connectors")
	}

	return &Cafica{
		connector: c,
	}, nil
}

// ExecSync execute a sync operation
func (c *Cafica) ExecSync(in data.Table, syncHandler SyncHandlerFunc) (data.Table, error) {

	client, err := c.connector.Connect()
	if err != nil {
		return nil, errors.Wrap(err, "could not create client")
	}
	defer client.Close()

	out, err := syncHandler(client, in)
	if err != nil {
		return nil, errors.Wrap(err, "function return with error")
	}

	return out, nil
}

// ForEachKeyAsync execute async iteration over each key
func (c *Cafica) ForEachKeyAsync(in data.Table, asyncHandler AsyncHandlerFunc) (
	<-chan data.Row, <-chan error) {

	size := len(in)
	rowCh := make(chan data.Row, size)
	errCh := make(chan error)

	go func(rowCh chan<- data.Row, errCh chan<- error) {
		wg := sync.WaitGroup{}
		defer close(rowCh)

		for topic := range in {

			wg.Add(1)
			go func(topic string) {
				defer wg.Done()

				client, err := c.connector.Connect()
				if err != nil {
					errCh <- errors.Wrapf(err, "could not create connectors for topic: %s", topic)
					return
				}
				defer client.Close()

				rows, err := asyncHandler(client, topic, -1)
				if err != nil {
					errCh <- errors.Wrapf(err, "function return with error on topic: %s", topic)
					return
				}

				for _, row := range rows {
					rowCh <- row
				}
			}(topic)
		}

		wg.Wait()

	}(rowCh, errCh)

	return rowCh, errCh
}

// ForEachRowAsync execute async iteration over each table row
func (c *Cafica) ForEachRowAsync(in data.Table, asyncHandler AsyncHandlerFunc) (
	<-chan data.Row, <-chan error) {

	size := len(in)
	rowCh := make(chan data.Row, size)
	errCh := make(chan error)

	go func(rowCh chan<- data.Row, errCh chan<- error) {
		wg := sync.WaitGroup{}
		defer close(rowCh)

		for topic, rows := range in {
			for _, row := range rows {

				wg.Add(1)
				go func(topic string, partition int32) {
					defer wg.Done()

					client, err := c.connector.Connect()
					if err != nil {
						errCh <- errors.Wrapf(err, "could not create connectors for topic: %s", topic)
						return
					}
					defer client.Close()

					rows, err := asyncHandler(client, topic, partition)
					if err != nil {
						errCh <- errors.Wrapf(err, "function return with error on topic: %s", topic)
						return
					}

					for _, row := range rows {
						rowCh <- row
					}
					return

				}(topic, row.Partition)
			}
		}

		wg.Wait()
	}(rowCh, errCh)

	return rowCh, errCh
}
