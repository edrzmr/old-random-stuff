package cafica

import "github.com/pkg/errors"

// Connector interface
type Connector interface {
	Connect() (Client, error)
}

// ConnectorType type of connector
type ConnectorType int

const (
	// SARAMA type that make connector create a sarama connector
	SARAMA ConnectorType = iota
)

// ErrUnknownConnectorType error for unknown connector type
var ErrUnknownConnectorType = errors.New("unknown connector type")

// NewConnector creates a new connector with given config and type
func NewConnector(config Config, connectorType ConnectorType) (Connector, error) {

	switch connectorType {
	case SARAMA:
		return NewSaramaConnector(config)
	}

	return nil, ErrUnknownConnectorType
}
