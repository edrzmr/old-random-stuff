package cafica

import (
	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
)

// SaramaClient implements Client interface
type SaramaClient struct {
	Client

	ID      uint32
	backend sarama.Client
}

// Topics return a list with kafka topics
func (sc *SaramaClient) Topics() ([]string, error) {
	return sc.backend.Topics()
}

// Partitions return a partition list for given topic
func (sc *SaramaClient) Partitions(topic string) (string, []int32, error) {
	partitions, err := sc.backend.Partitions(topic)
	return topic, partitions, err
}

// Offsets return the newest and oldest offsets for given partition
func (sc *SaramaClient) Offsets(topic string, partition int32) (
	topicName string, partitionID int32, newest int64, oldest int64, err error) {

	newest, err = sc.backend.GetOffset(topic, partition, sarama.OffsetNewest)
	if err != nil {
		return topic, partition, -1, -1, errors.Wrapf(
			err, "could not get newest offset [%s:%d]\n", topic, partition)
	}

	oldest, err = sc.backend.GetOffset(topic, partition, sarama.OffsetOldest)
	if err != nil {
		return topic, partition, -1, -1, errors.Wrapf(
			err, "could not get oldest offset [%s-%d]\n", topic, partition)
	}

	return topic, partition, newest, oldest, nil
}

// Close connection with broker
func (sc *SaramaClient) Close() error {
	return sc.backend.Close()
}
