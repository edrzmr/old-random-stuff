package handler

import (
	"github.com/drrzmr/cafica/cafica"
	"github.com/drrzmr/cafica/cafica/data"
	"github.com/drrzmr/cafica/cafica/data/rows"
	"github.com/drrzmr/cafica/cafica/data/table"
	"github.com/drrzmr/cafica/logger"
)

// QueryTopics query topics from a connection and create a data.Table
func QueryTopics(conn cafica.Connection, _ data.Table) (data.Table, error) {

	log := logger.Get()
	topics, err := conn.Topics()
	if err != nil {
		log.Error(err)
	} else {
		for _, topic := range topics {
			log.Debugf("Query Topics [%s]", topic)
		}
	}
	return table.WithTopics(topics, err)
}

// QueryPartitions query partitions from connection and create a data.Rows
func QueryPartitions(conn cafica.Connection, topic string, _ int32) (data.Rows, error) {

	log := logger.Get()
	topic, partitions, err := conn.Partitions(topic)
	if err != nil {
		log.Error(err)
	} else {
		for _, partition := range partitions {
			log.Debugf("Query Partitions [%s:%d]", topic, partition)
		}
	}
	return rows.WithPartitions(topic, partitions, err)
}

// QueryOffsets query offsets from connection and create a [1]data.Row
func QueryOffsets(conn cafica.Connection, topic string, partition int32) (data.Rows, error) {

	log := logger.Get()
	_, _, newest, oldest, err := conn.Offsets(topic, partition)
	if err != nil {
		log.Error(err)
	} else {
		log.Debugf("Query Offsets [%s:%d:%d:%d]", topic, partition, newest, oldest)
	}
	return rows.WithOffsets(topic, partition, newest, oldest, err)
}
