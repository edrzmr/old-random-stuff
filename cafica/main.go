package main

import (
	"fmt"
	"os"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/drrzmr/cafica/cafica"
	"github.com/drrzmr/cafica/cafica/calc"
	"github.com/drrzmr/cafica/cafica/data"
	"github.com/drrzmr/cafica/cafica/data/table"
	"github.com/drrzmr/cafica/command"
	"github.com/drrzmr/cafica/handler"
	"github.com/drrzmr/cafica/logger"
)

type exitCode int

const (
	exitOk exitCode = iota
	exitKafkaCreateFactoryError
	exitKafkaExecutorError
	exitCommandError
)

func exit(code exitCode, err error) {
	if err != nil {
		logger.Get().Error(err)
		os.Exit(int(code))
	}
	if code == exitOk {
		os.Exit(int(code))
	}
}

// nolint: dupl, gocyclo
func main() {

	spew.Config.DisablePointerAddresses = true
	spew.Config.DisableCapacities = true
	spew.Config.SortKeys = true
	spew.Config.Indent = "|  "

	exit(exitCommandError, command.Run(os.Args[1:], func(context *command.Context) error {

		config := context.Config()
		command := context.Command()
		log := context.Log()

		spew.Dump(config)

		client, err := cafica.New(config.Kafka, config.Kafka.Backend)
		exit(exitKafkaCreateFactoryError, err)

		switch command {
		case "topic.list":
			log.Info("loading topics...")
			tableWithTopics, err := client.ExecSync(nil, handler.QueryTopics)
			exit(exitKafkaExecutorError, err)

			if context.Flag("with.size") {

				tableWithPartitions := table.New(nil)
				rowCh, errCh := client.ForEachKeyAsync(tableWithTopics, handler.QueryPartitions)
				var row data.Row
				for ok := true; ok; {
					select {
					case err := <-errCh:
						log.Error(err)
					case row, ok = <-rowCh:
						if ok {
							tableWithPartitions.Append(row)
						}
					default:
						log.Info("loading topic partitions...")
						time.Sleep(2 * time.Second)
					}
				}

				tableWithRange := table.New(nil)
				rowCh, errCh = client.ForEachRowAsync(tableWithPartitions, handler.QueryOffsets)
				for ok := true; ok; {
					select {
					case err := <-errCh:
						log.Error(err)
					case row, ok = <-rowCh:
						if ok {
							tableWithRange.Append(row)
						}
					default:
						log.Info("loading partitions range...")
						time.Sleep(2 * time.Second)
					}
				}

				groupBy := calc.GroupByTopic(tableWithRange)

				for _, group := range groupBy {
					fmt.Printf("%s,%d\n", group.Topic, group.Length)
				}

				/* just list topics */
			} else {
				for topicName := range tableWithTopics {
					fmt.Println(topicName)
				}
			}

		}

		return nil
	}))

	exit(exitOk, nil)
}
