package logger

import (
	"sync"

	"go.uber.org/zap/zapcore"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// nolint: gochecknoglobals
var (
	once sync.Once
	log  *zap.SugaredLogger
)

// Logger define the log interface
type Logger interface {
	Warn(args ...interface{})
	Info(args ...interface{})
	Error(args ...interface{})
	Debugf(template string, args ...interface{})
}

// LogLevel of application log
type LogLevel int

const (
	// DebugLevel logs are typically voluminous, and are usually disabled in production.
	DebugLevel LogLevel = iota
	// InfoLevel is the default logging priority.
	InfoLevel
	// WarnLevel logs are more important than Info, but don't need individual human review.
	WarnLevel
	// ErrorLevel logs are high-priority. If an application is running smoothly, it shouldn't generate any error-level
	// logs.
	ErrorLevel
	// DPanicLevel logs are particularly important errors. In development the logger panics after writing the message.
	DPanicLevel
	// PanicLevel logs a message, then panics.
	PanicLevel
	// FatalLevel logs a message, then calls os.Exit(1).
	FatalLevel
)

// Level return a LogLevel for given string
func Level(level string) LogLevel {
	switch level {
	case "debug":
		return DebugLevel
	case "warn":
		return WarnLevel
	case "error":
		return ErrorLevel
	case "dpanic":
		return DPanicLevel
	case "panic":
		return PanicLevel
	case "fatal":
		return FatalLevel
	case "info":
		fallthrough
	default:
		return InfoLevel

	}
}

// String return string version of Level
func (level LogLevel) String() string {
	switch level {
	case DebugLevel:
		return "debug"
	case WarnLevel:
		return "warn"
	case ErrorLevel:
		return "error"
	case DPanicLevel:
		return "dpanic"
	case PanicLevel:
		return "panic"
	case FatalLevel:
		return "fatal"
	case InfoLevel:
		fallthrough
	default:
		return "info"
	}
}

func (level LogLevel) toZap() zapcore.Level {
	switch level {
	case DebugLevel:
		return zap.DebugLevel
	case WarnLevel:
		return zap.WarnLevel
	case ErrorLevel:
		return zap.ErrorLevel
	case DPanicLevel:
		return zap.DPanicLevel
	case PanicLevel:
		return zap.PanicLevel
	case FatalLevel:
		return zap.FatalLevel
	case InfoLevel:
		fallthrough
	default:
		return zap.InfoLevel
	}
}

// Configure logger
func Configure(level LogLevel) error {

	var err error

	once.Do(func() {
		zapConfig := zap.NewDevelopmentConfig()
		zapConfig.Level = zap.NewAtomicLevelAt(level.toZap())
		zapConfig.ErrorOutputPaths = []string{"stderr"}
		zapConfig.OutputPaths = []string{"stderr"}

		var logger *zap.Logger

		logger, err = zapConfig.Build()
		if err != nil {
			return
		}
		log = logger.Sugar()
	})

	return errors.Wrap(err, "could not create logger")
}

// Get log
func Get() Logger {
	if log != nil {
		return log
	}

	err := Configure(DebugLevel)
	if err != nil {
		panic(err)
	}
	log.Warn("using unconfigured logger")
	return log
}
