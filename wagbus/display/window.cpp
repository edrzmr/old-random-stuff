#include "window.h"
#include "ui_window.h"

#include <stdio.h>

Window::Window(QWidget * parent) : QMainWindow(parent), ui(new Ui::Window)
{
	ui->setupUi(this);
	ui->comboBoxPorta->addItem("/dev/ttyS0", "/dev/ttyS0");
	ui->comboBoxPorta->addItem("/dev/ttyS1", "/dev/ttyS1");
	ui->comboBoxPorta->addItem("/dev/ttyS2", "/dev/ttyS2");
	ui->comboBoxPorta->addItem("/dev/ttyS3", "/dev/ttyS3");
	connect(ui->actionConectar, SIGNAL(triggered()),
			this, SLOT(on_pushButtonConectar_clicked()));
}

Window::~Window()
{
	delete ui;
}

void
Window::desconectar()
{
	ui->statusBar->showMessage("Desconectado", 0);
	ui->labelMensagem->setText("Desconectado");

	ui->comboBoxPorta->setDisabled(false);
	ui->spinBoxBuffer->setDisabled(false);
	ui->spinBoxSegundos->setDisabled(false);
	ui->spinBoxMilisegundos->setDisabled(false);

	ui->pushButtonConectar->setText("Conectar");
	ui->actionConectar->setText("Conectar");

	disconnect(ui->pushButtonConectar, SIGNAL(clicked()),
			   this, SLOT(desconectar()));
	disconnect(ui->actionConectar, SIGNAL(triggered()),
			   this, SLOT(desconectar()));

	connect(ui->pushButtonConectar, SIGNAL(clicked()),
			this, SLOT(on_pushButtonConectar_clicked()));
	connect(ui->actionConectar, SIGNAL(triggered()),
			this, SLOT(on_pushButtonConectar_clicked()));
}

void
Window::on_pushButtonConectar_clicked()
{
	QString msg = QString();

	msg = "porta: " + ui->comboBoxPorta->currentText() + " | ";
	msg += "tamanho do buffer: " + ui->spinBoxBuffer->text() + " | ";
	msg += "segundos: " + ui->spinBoxSegundos->text() + " | ";
	msg += "milisegundos: " + ui->spinBoxMilisegundos->text();
	ui->statusBar->showMessage(msg, 0);
	ui->labelMensagem->setText(msg);

	ui->comboBoxPorta->setDisabled(true);
	ui->spinBoxBuffer->setDisabled(true);
	ui->spinBoxSegundos->setDisabled(true);
	ui->spinBoxMilisegundos->setDisabled(true);

	ui->pushButtonConectar->setText("Desconectar");
	ui->actionConectar->setText("Desconectar");

	disconnect(ui->pushButtonConectar, SIGNAL(clicked()),
			   this, SLOT(on_pushButtonConectar_clicked()));
	disconnect(ui->actionConectar, SIGNAL(triggered()),
			   this, SLOT(on_pushButtonConectar_clicked()));

	connect(ui->pushButtonConectar, SIGNAL(clicked()),
			this, SLOT(desconectar()));
	connect(ui->actionConectar, SIGNAL(triggered()), this, SLOT(desconectar()));
}
