#ifndef WINDOW_H
#define WINDOW_H

#include <QtGui/QMainWindow>

namespace Ui
{
	class Window;
}

class Window:public QMainWindow
{
	Q_OBJECT

public:
	Window(QWidget *parent = 0);
	~Window();

private slots:
	void on_pushButtonConectar_clicked();
	void desconectar();

private:
	Ui::Window *ui;
};

#endif // WINDOW_H
