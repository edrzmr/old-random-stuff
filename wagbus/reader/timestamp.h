#ifndef __TIMESTAMP_H
#define __TIMESTAMP_H

#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C"
{
#endif

	extern uint64_t getTimeMiliseconds();

#ifdef __cplusplus
}
#endif

#endif
