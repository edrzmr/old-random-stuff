#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>

#include "timestamp.h"

#include "wagbus.h"
#include "sensor.h"

int
wagbus_serial_connect(char *port, speed_t baudrate)
{
	struct termios termios;
	int fd = open(port, O_RDWR);

	if (fd < 0) {
		return -1;
		//perror("Prolema em: problema sem acessar a porta");
		//exit(1);
	}
	tcgetattr(fd, &termios);	// salva as configuracoes da porta
	cfmakeraw(&termios);		// seta a porta pra raw mode
	cfsetspeed(&termios, baudrate);	// seta a velocidade da porta
	tcsetattr(fd, TCIOFLUSH, &termios);	// aplica as configuracoes na porta
	tcflush(fd, TCIFLUSH);		// flush no buffer da porta

	return fd;
}

struct wagbus *
wagbus_connect(char *port, speed_t baudrate, int size_buffer, int usec)
{
	if (port) {

		struct wagbus *wagbus = create(struct wagbus);
		wagbus->fd = wagbus_serial_connect(port, baudrate);
		wagbus->size_buffer = size_buffer;
		wagbus->sec = 0;
		wagbus->usec = usec;
		wagbus->buffer = NULL;
		wagbus->list = NULL;
		wagbus->timestamp = -1;

		wagbus->sensor_array = sensor_create(ARRAY_SENSOR_MAX);

		return wagbus;
	}
	return NULL;
}

struct wagbus *
wagbus_watch_port(struct wagbus *w)
{
	int bytes = 0;
	struct timeval *timer = create(struct timeval);
	//if (w->timestamp == 0xffffffffffffffffLL)
	w->timestamp = getTimeMiliseconds();
	do {
		timer->tv_sec = w->sec;
		timer->tv_usec = w->usec;
		//select(0,NULL,NULL,NULL,timer);
		usleep(timer->tv_usec);
		ioctl(w->fd, FIONREAD, &bytes);
#if DEBUG >= 10
		printf("buffer: %i\n", bytes);
#endif

	} while (bytes <= w->size_buffer);

	return w;
}

struct wagbus *
wagbus_buffer_parser(struct wagbus *w)
{
	int bytes;
	ioctl(w->fd, FIONREAD, &bytes);
	struct buffer *tmp_buffer = buffer_create(bytes);
	tmp_buffer->load(tmp_buffer, w->fd);

	if (w->buffer) {

		if (w->buffer->lenght > 0) {

#if DEBUG >= 10
			printf("\ntemos buffer ainda, buffer: %i ", w->buffer->lenght);
			buffer_print(w->buffer->data, 0, w->buffer->lenght);
			printf("\n\n");
#endif
			struct buffer *b = buffer_concat(w->buffer, tmp_buffer);
#if DEBUG >= 10
			printf("buffer novo: ");
			buffer_print(tmp_buffer->data, 0, 30);
			printf("...\n");
			printf("buffer concatenado: ");
			buffer_print(b->data, 0, 30);
			printf("...\n\n");
			sleep(5);
#endif
			tmp_buffer = buffer_delete(tmp_buffer);
			w->buffer = b;

			return w;
		}
	}
	w->buffer = tmp_buffer;
	return w;
}

struct wagbus *
wagbus_package_parser(struct wagbus *w)
{
	struct buffer *b = w->buffer;
	struct list *list = (w->list != NULL) ? w->list : list_init();
	int pkg_size = 0;
	//uint64_t timestamp = getTimeMiliseconds();
	uint32_t i = 0;
	while (i < b->lenght) {

		if (b->data[i] == START_VALUE) {

			// verifica se tem ao menos um header no buffer
			if (b->lenght - i >= sizeof(struct header)) {

				pkg_size = sizeof(struct header) + b->data[i + LENGHT] + 1;
				// verifica se tem um pacote inteiro no buffer
				if ((int)(b->lenght - i) >= pkg_size) {

					struct package *p = package_create();
					p = package_make(p, &(b->data[i]));
#if DEBUG >=1
					package_print(p);
#if DEBUG < 9
					printf("\n");
#endif // DEBUG < 9
#endif // DEBUG >= 1
#if DEBUG >= 9
					printf
						(" pkg_size: %i, b->lenght-i: %i, b->data[i]: %.02X | ",
						 pkg_size, b->lenght - i, b->data[i]
						);
					buffer_print(b->data, i, pkg_size + i);
					printf("\n");
#endif
					list->push_back(list, (void *)p);
//                  p = package_delete(p);
					i += pkg_size;
					w->pkg_count++;

				} else {		// buffer insuficiente para pacote
					break;
				}
			} else {			// buffer insuficiente para reader
				break;
			}
		} else {				//bytes nao identificados no buffer
#if DEBUG >= 2
			printf("start byte invalido: %.02X\n", b->data[i]);
#endif
			w->pkg_invalid_byte++;
			i++;
		}
	}

	if (i == b->lenght) {
#if DEBUG >= 10
		printf("saida normal :)\n");
#endif
		w->buffer = buffer_flush(w->buffer);
		// temos pacotes fragmentados :/
	} else {
#if DEBUG >= 10
		printf("buffer menor q header ou pacote\n");
		printf("pkg_size: %i, b->lenght-i: %i, b->data[i]: %.02X | ",
			   pkg_size, b->lenght - i, b->data[i]
			);
		buffer_print(b->data, i, b->lenght);
		printf("\n");
#endif
		b = buffer_trunc(b, i);
#if DEBUG >= 10
		printf("buffer truncado\n");
		buffer_print(b->data, 0, b->lenght);
		printf("\n");
#endif
		w->pkg_fragment++;
	}
	// w->list = list_flush(w->list);
	w->list = list;
	return w;
}

struct wagbus *
wagbus_parser(struct wagbus *w)
{
	if (w) {
		w = wagbus_buffer_parser(w);
		w = wagbus_package_parser(w);
		return w;
	}
	return NULL;
}

void
wagbus_list_print(struct wagbus *w)
{
	struct package *p = NULL;
	struct node *it = NULL;
	struct list *list = w->list;

	if (list) {

		list->reset_it(list);
		while ((it = list->get_it(list))) {

			p = (struct package *)list->get_data_from_it(it);

			package_print(p);
			printf("\n");
		}
#if DEBUG >= 10
		printf("tamanho da lista: %i\n", list->lenght);
#endif
	}
}

void
wagbus_flush_list(struct wagbus *w)
{
	w->list = list_flush(w->list);
}
