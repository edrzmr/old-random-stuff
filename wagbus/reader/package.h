#ifndef __PACKAGE_H__
#define __PACKAGE_H__

#include <stdint.h>

#define create(x) (x*) malloc(sizeof(x))

#define VERMELHO "\033[31;1m"
#define VERDE "\033[32;1m"
#define AMARELO "\033[33;1m"
#define NORMAL "\033[m"

#define START 0
#define SEQ 1
#define ID 2
#define TYPE 3
#define LENGHT 4

#define START_VALUE 0x0A
//#define START_VALUE 0x8A

struct header
{
	uint8_t start;
	uint8_t package_seq;
	uint8_t sensor_id;
	uint8_t data_type;
	uint8_t data_lenght;
};

struct payload
{
	uint8_t *data;
	uint8_t crc;
};

struct package
{
	struct header *header;
	struct payload *payload;

};

struct package *package_create();

struct package *package_make(struct package *p, uint8_t * from);

struct package *package_print(struct package *package);

uint8_t package_calc_crc(struct package *p);

struct package *package_delete(struct package *p);

struct package *package_clone(struct package *p);

#endif // __PACKAGE_H__
