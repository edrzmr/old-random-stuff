#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "buffer.h"

void
buffer_print(uint8_t * b, int ini, int fim)
{
	int i;

	printf("[");
	for (i = ini; i < fim - 1; i++) {

		printf("%.02X ", b[i]);
	}
	printf("%.02X]", b[i]);
}

void
buffer_parser(struct buffer *buffer)
{
	if ((buffer) && (buffer->data)) {

		uint32_t i;
		for (i = 0; i < buffer->lenght; i++) {

			if (buffer->data[i] == 0x0A) {
			// if (buffer->data[i] == 0x8A){

				printf("\n");
			}
			printf("%.2X ", buffer->data[i]);
		}
		printf("*\n\n");
	}
}

struct buffer *
buffer_create(unsigned int lenght)
{
	if ((int)lenght >= 0) {

		struct buffer *buffer = create(struct buffer);
		buffer->data = (uint8_t *) malloc(sizeof(uint8_t) * lenght);
		buffer->lenght = lenght;

		buffer->load = buffer_load;
		buffer->parser = buffer_parser;
		buffer->delete = buffer_delete;

		return buffer;
	}
	return NULL;
}

struct buffer *
buffer_delete(struct buffer *buffer)
{
	if ((buffer) && (buffer->data)) {

		free(buffer->data);
		free(buffer);
		buffer = NULL;
		return NULL;
	}
	// return buffer;
	return NULL;
}

struct buffer *
buffer_flush(struct buffer *buffer)
{
	if (buffer) {
		if (buffer->data) {
			free(buffer->data);
			buffer->data = NULL;
			buffer->lenght = 0;
		}
	}
	return buffer;
}

struct buffer *
buffer_load(struct buffer *buffer, int fd)
{
	if (buffer == NULL) {
		return buffer;
	} else if (buffer->lenght > 0 && buffer->data) {
		int ret = read(fd, buffer->data, buffer->lenght);
		if (ret == (int)buffer->lenght) {
			return buffer;
		} else {
			buffer = buffer_delete(buffer);
			return NULL;
		}
	}
	return NULL;
}

struct buffer *
buffer_concat(struct buffer *b1, struct buffer *b2)
{
	if ((b1) && (b2)) {
		struct buffer *buffer = buffer_create(b1->lenght + b2->lenght);
		memcpy(buffer->data, b1->data, b1->lenght);
		memcpy(&(buffer->data[b1->lenght]), b2->data, b2->lenght);
		return buffer;
	}
	return NULL;
}

struct buffer *
buffer_trunc(struct buffer *b, int index)
{
	if ((int) b->lenght > index) {
		int lenght = b->lenght - index;
		uint8_t *data = (uint8_t *) malloc(sizeof(uint8_t) * lenght);
		memcpy(data, &(b->data[index]), lenght);
		free(b->data);
		b->data = data;
		b->lenght = lenght;
	}
	return b;
}
