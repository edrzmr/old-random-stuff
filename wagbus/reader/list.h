#ifndef _LIST_H
#define _LIST_H

#include "node.h"

// representa uma lista, seus atributos e metodos
struct list
{
	struct node *head;
	struct node *end;
	struct node *it;
	unsigned int lenght;

	// ponteiro pra funcoes 
	struct node *(*push_back) ();
	struct node *(*push_front) ();
	void *(*get_data_from_it) ();
	void (*reset_it) ();
	struct node *(*get_it) ();
	void *(*get_data) ();
	struct node *(*node_init) ();
};

// inicializa um list e faz o link entre os ponteiros pra funcao e os metodos
// retorna um ponteiro para a lista criada
struct list *list_init(void);

// adiciona data a um node da lista
// retorna um ponteiro para o node inserido
struct node *push_back(struct list *list, void *data);

struct node *push_front(struct list *list, void *data);

void *get_data_from_it(struct node *it);

struct node *get_it(struct list *list);

void reset_it(struct list *list);

struct list *list_flush(struct list *list);

struct list *list_clear(struct list *list);

#endif // _LIST_H
