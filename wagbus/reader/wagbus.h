#ifndef __WAGBUS_H__
#define __WAGBUS_H__

#include "list.h"
#include "buffer.h"
#include "package.h"
#include "sensor.h"

struct wagbus
{
	int size_buffer;
	int sec;
	int usec;
	int fd;
	struct buffer *buffer;
	struct list *list;
	unsigned long int pkg_count;
	unsigned long int pkg_fragment;
	unsigned long int pkg_invalid_byte;
	uint64_t timestamp;
	struct sensor *sensor_array;
};

int wagbus_serial_connect(char *port, speed_t baudrate);

//struct buffer*
struct wagbus *wagbus_watch_port(struct wagbus *w);

struct list *wagbus_buffer_2_list(struct wagbus *w);

struct wagbus *wagbus_parser(struct wagbus *w);

struct wagbus *wagbus_buffer_parser(struct wagbus *w);

struct wagbus *wagbus_package_parser(struct wagbus *w);

void wagbus_list_print(struct wagbus *w);

struct wagbus *wagbus_connect(char *port, speed_t baudrate, int size_buffer,
							  int usec);

void wagbus_flush_list(struct wagbus *w);

#endif // __WAGBUS_H__
