#ifndef _NODE_H
#define _NODE_H

// representa um no da lista
struct node
{
	// atributos
	struct node *next;
	void *data;
};

// inicializa um node e faz o link entre os ponteiros pra funcao e os metodos
// retorna um ponteiro para o node criado
struct node *node_init(void *data);

#endif
