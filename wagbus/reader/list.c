#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "list.h"

#define create(x) (x*) malloc(sizeof(x))

struct list *
list_init(void)
{
	struct list *list = create(struct list);

	// inicializando a cabeca da lista
	list->head = NULL;
	list->end = NULL;
	list->lenght = 0;

	// lincando as funcoes da lista
	list->push_back = push_back;
	list->push_front = push_front;
	list->get_data_from_it = get_data_from_it;
	list->get_it = get_it;
	list->reset_it = reset_it;

	list->node_init = node_init;
	return list;
}

struct list *
list_flush(struct list *list)
{
	if (list) {
		while (list->head) {
			list->it = list->head;
			list->head = list->head->next;

			void *data = list->it->data;
			free(data);
			data = NULL;
			free(list->it);
			list->it = NULL;
		}

		list->it = NULL;
		list->head = NULL;
		list->end = NULL;
		list->lenght = 0;
		free(list);
		list = NULL;
		return list;
	}
	return NULL;
}

struct list *
list_clear(struct list *list)
{

	if (list) {
		while (list->head) {
			list->it = list->head;
			list->head = list->head->next;

			void *data = list->it->data;
			free(data);
			data = NULL;
			free(list->it);
			// list->it = NULL;
		}

		list->it = NULL;
		list->head = NULL;
		list->end = NULL;
		list->lenght = 0;
		// free(list);
		// list = NULL;
		return list;
	}
	return NULL;
}

struct node *
push_back(struct list *list, void *data)
{
	assert(data != NULL);

	if (list->head == NULL) {

		list->head = list->node_init(data);
		list->end = list->head;
		list->it = list->head;
		list->lenght++;
		return list->head;

	} else {

		list->end->next = list->node_init(data);
		list->end = list->end->next;
		list->lenght++;
		return list->end;
	}
}

struct node *
push_front(struct list *list, void *data)
{
	struct node *node = NULL;

	assert(data != NULL);

	if (list->head == NULL) {

		list->head = list->node_init(data);
		list->end = list->head;
		list->it = list->head;
		return list->head;

	} else {

		node = list->node_init(data);
		node->next = list->head;
		list->head = node;
		return list->head;
	}
}

void
reset_it(struct list *list)
{
	list->it = list->head;
}

void *
get_data_from_it(struct node *it)
{
	return it->data;
}

struct node *
get_it(struct list *list)
{

	struct node *it = list->it;

	if (list->it)
		list->it = list->it->next;
	return it;
}
