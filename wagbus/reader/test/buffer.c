#include "../buffer.h"

#include <stdio.h>

int
main()
{
	struct buffer *b1 = buffer_create(5);
	struct buffer *b2 = buffer_create(5);

	b1->data[0] = 0;
	b1->data[1] = 1;
	b1->data[2] = 1;
	b1->data[3] = 1;
	b1->data[4] = 1;

	b2->data[0] = 2;
	b2->data[1] = 3;
	b2->data[2] = 4;
	b2->data[3] = 4;
	b2->data[4] = 4;

	struct buffer *b = buffer_create(b1->lenght + b2->lenght);

	b = buffer_concat(b1, b2);

	buffer_print(b1->data, 0, b1->lenght);
	buffer_print(b2->data, 0, b2->lenght);
	buffer_print(b->data, 0, b->lenght);

	b = buffer_trunc(b, 8);

	buffer_print(b->data, 0, b->lenght);

	return 0;
}
