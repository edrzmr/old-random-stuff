#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int
main(void)
{
	float f;
	short int i;
	uint8_t byte[6] = { 0x1A, 0x1C, 0x01, 0x88, 0x00, 0x00 };
	i = (byte[0] << 8) + byte[1];
	f = i * 0.28;
	printf("distancia = %f\n", f);
	i = (byte[2] << 8) + byte[3];
	f = i * 0.28;
	printf("velocidade = %f\n", f);
	i = (byte[4] << 8) + byte[5];
	f = i * 0.28;
	printf("aceleracao = %f\n", f);
	return 0;
}
