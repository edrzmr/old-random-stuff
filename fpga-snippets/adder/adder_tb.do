vlib work
vlog adder.v adder_tb.v
vsim -t ns work.adder_tb
view wave
add wave -radix hex /dataa
add wave -radix hex /datab
add wave -radix hex /out
run 10 ns
