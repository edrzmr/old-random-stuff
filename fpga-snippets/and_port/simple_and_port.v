module simple_and_port(
	input  KEY0,
	input  KEY1,
	output LED0
);

assign LED0 = (!KEY0 && !KEY1);

endmodule
