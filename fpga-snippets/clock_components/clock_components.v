module clock_components(
	input       CLOCK_50MHZ,
	input       KEY0,
	output [7:0]LED
);

wire [31:0] counter;
wire reset;
wire enable;

assign LED[7:0] = counter[31:24];

debounce debounce_inst(
	.CLOCK (CLOCK_50MHZ),
	.IN    (KEY0),
	.OUT   (enable)
);

reset_gen reset_gen_inst(
	.CLOCK  (CLOCK_50MHZ),
	.IN     (enable),
	.OUT    (reset)
);

counter counter_inst(
	.CLOCK (CLOCK_50MHZ),
	.ENABLE(enable),
	.RESET (reset),
	.OUT   (counter)
);

endmodule
