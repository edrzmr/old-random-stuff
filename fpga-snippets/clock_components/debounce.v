module debounce(
	input  CLOCK,
	input  IN,
	output OUT
);

reg r0;
reg r1;

assign OUT = r1;

always @(posedge CLOCK, negedge IN)
begin
	if (0 == IN) begin
		r0 <= 0;
		r1 <= 0;
	end else begin
		r0 <= 1;
		r1 <= r0;
	end
end

endmodule
