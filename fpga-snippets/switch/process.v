module process(
	input  [3:0] SW,
	input  [1:0] KEY,
	output [7:0] LED
);

/*
assign LED0 = SW0;
assign LED1 = SW1;
*/

reg led;

assign LED[0] = led;
assign LED[1] = KEY[0] == 0 ? ~SW[0] && ~SW[1] : ~SW[0] || ~SW[1];
assign LED[7] = ~SW[3];

always @(SW[0], SW[1], KEY[0])
begin
	if (0 == KEY[0]) begin
		led <= ~SW[0] && ~SW[1];
	end else begin
		led <= ~SW[0] || ~SW[1];
	end
end

endmodule
