#ifndef __MATRIX__
#define __MATRIX__

#include <stdbool.h>

#include "common.h"
#include "pixel.h"

BEGIN_DECLS

typedef struct matrix matrix_t;

matrix_t *
matrix_new(const unsigned rows, const unsigned cols);

void
matrix_free(matrix_t *m);

bool
matrix_set(matrix_t *m, const unsigned row, const unsigned col, int data);

void
matrix_print(const matrix_t *m);

void
matrix_print_as_int(const matrix_t *m);

bool
matrix_is_equal(const matrix_t *m1, const matrix_t *m2, unsigned rows, unsigned cols);

unsigned
matrix_get_rows(matrix_t *m);

unsigned
matrix_get_cols(matrix_t *m);

const unsigned *
matrix_get_data(const matrix_t *m);

unsigned *
matrix_get_data_writeble(matrix_t *m);

matrix_t *
matrix_zoom_in(const matrix_t *m, unsigned fator, matrix_t *zoom);

unsigned int
matrix_get_size(matrix_t *m);

END_DECLS

#endif /* __MATRIX__ */
