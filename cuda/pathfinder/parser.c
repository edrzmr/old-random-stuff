#include "parser.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define FILE_BUF_SIZE (10 + 1)

typedef struct
{
	char *data;
	unsigned size;
	unsigned len;

} buffer_t;

static buffer_t *
buffer_new(unsigned size)
{
	buffer_t *b = (buffer_t *) malloc(sizeof(buffer_t));
	b->data = (char *) calloc(size, sizeof(char));
	b->size = size;
	b->len = 0;

	return b;
}

static void
buffer_free(buffer_t *b)
{
	if (NULL != b) {
		free(b->data);
	}

	free(b);
}

static void
buffer_concat(buffer_t *b, const char *data, unsigned len)
{
	unsigned free_buf = b->size - b->len;

	if (free_buf < len) {

		b->size += len - free_buf;
		b->data = (char *) realloc(b->data, b->size);
		if (NULL == b->data) {
			log("ops! NULL == b->data");
			exit(-69);
		}
	}

	memcpy(b->data + b->len, data, len);
	b->len += len;
}

typedef bool (*func_t)(const unsigned count, const char *ini, const char *end, void *user_data);

static bool
f(const unsigned count, const char *ini, const char *end, void *user_data)
{
	int *size = (int *) user_data;

	size[0] = count+1;
	size[1] = (end-ini > size[1]) ? end-ini : size[1];

	return true;
}

static bool
f2(const unsigned row, const char *ini, const char *end, void *user_data)
{
	unsigned col = 0;
	matrix_t *m = (matrix_t *) user_data;

	for (; ini < end; ini++, col++) {

		switch(*ini) {
		case '|':
			matrix_set(m, row, col, BLACK);
			break;
		case '_':
			matrix_set(m, row, col, BLACK);
			break;
		case ' ':
			matrix_set(m, row, col, WHITE);
			break;
		case '*':
			matrix_set(m, row, col, BLUE);
			break;
		case 'O':
			matrix_set(m, row, col, RED);
			break;
		default:
			matrix_set(m, row, col, GREEN);
			break;
		}
	}

	return true;
}

static inline const char *
next_end_pointer(const char *ini)
{
	const char *end = strchr(ini, '\n');
	if (NULL == end) {
		end = strchr(ini, '\0');
	}

	if (NULL == end) {
		log("bug!");
		return NULL;
	}

	return end;
}

static inline const char *
next_ini_pointer(const char *end)
{
	if ('\0' == *end)
		return end;

	return end+1;
}

static bool
buffer_foreach(const buffer_t *b, func_t f, void *user_data)
{
	const char *ini = b->data;
	const char *end = NULL;
	unsigned count = 0;

	while ('\0' != *ini) {

		end = next_end_pointer(ini);

		/* ignore blank and comented lines */
		if ('#' == *ini || '\n' == *ini) {
			ini = next_ini_pointer(end);
			continue;
		}

		if (false == f(count++, ini, end, user_data))
			return false;

		ini = next_ini_pointer(end);
	}

	return true;
}

static matrix_t *
buffer_get_matrix(buffer_t *b)
{
	int size[2] = { 0, 0 };
	matrix_t *m = NULL;

	buffer_foreach(b, f, size);
	m = matrix_new(size[0], size[1]);

	buffer_foreach(b, f2, m);

	return m;
}

static buffer_t *
load_file_to_buffer(const char *filename)
{
	FILE *file = NULL;
	char *tmp = NULL;
	buffer_t *buf = NULL;
	size_t len;

	file = fopen(filename, "r");
	if (NULL == file) {
		log("could not open file: %s", filename);
		return NULL;
	}

	buf = buffer_new(FILE_BUF_SIZE);

	tmp = (char *) calloc(FILE_BUF_SIZE, sizeof(char));
	while (false == feof(file)) {

		len = fread(tmp, 1, sizeof(tmp), file);
		buffer_concat(buf, tmp, len);
	}
	tmp[0] = '\0';
	buffer_concat(buf, tmp, 1);

	free(tmp);
	fclose(file);
	return buf;
}

matrix_t *
parser_run(const char *filename)
{
	buffer_t *buf = NULL;
	matrix_t *m = NULL;

	buf = load_file_to_buffer(filename);
	if (NULL == buf) {
		return NULL;
	}

	m = buffer_get_matrix(buf);
	if (NULL == m) {
		log("could not get matrix from buffer");
		return NULL;
	}

	buffer_free(buf);

	return m;
}

#ifdef __TEST__
int
main(void)
{
	log("vida loka!");
}
#endif /* __TEST__ */
