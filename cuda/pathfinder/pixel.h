#ifndef __PIXEL__
#define __PIXEL__

#include <stdint.h>

BEGIN_DECLS

#define WHITE 0x00FFFFFF
#define BLACK 0x00000000
#define BLUE  0x00FF0000
#define GREEN 0x0000FF00
#define RED   0x000000FF
#define YELLOW 0x0000FFFF

typedef union
{
	struct {
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
	} color;
	uint32_t i;
} pixel_t;

END_DECLS

#endif /* __PIXEL__ */
