#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <cuda_runtime.h>

#include "matrix.h"
#include "parser.h"
#include "common.h"

#include "gluda.h"

#include "cuda-common.h"

#define DEFAULT_ZOOM_IN 50

static unsigned _zoom_in = DEFAULT_ZOOM_IN;
static unsigned *_in = NULL;
static unsigned *_parent = NULL;
static unsigned *_found = NULL;

typedef struct
{
	matrix_t *m;
	matrix_t *p;

} container_t;

__device__ static inline unsigned
getpos(unsigned x, unsigned y, unsigned cols)
{
	return x + y * cols;
}

__device__ static inline void
go(unsigned *in, unsigned *parent, unsigned pos, unsigned ant, unsigned *found)
{
	if (WHITE == in[pos]) {
		parent[pos] = ant;
		in[pos] = BLUE;
	}

	if (RED == in[pos]) {
		parent[pos] = ant;
		in[pos] = YELLOW;
		*found = pos;
	}
}

__global__ void
kernel_blink(unsigned *in, unsigned *parent, unsigned rows, unsigned cols, unsigned *found)
{
	unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	unsigned y = threadIdx.y + blockIdx.y * blockDim.y;

	unsigned pos =  getpos(x,   y,   cols);

	/* TODO validate overflows */
	unsigned ltop = getpos(x-1, y-1, cols);
	unsigned lmid = getpos(x-1, y,   cols);
	unsigned lbot = getpos(x-1, y+1, cols);

	unsigned mtop = getpos(x,   y-1, cols);
	unsigned mbot = getpos(x,   y+1, cols);

	unsigned rtop = getpos(x+1, y-1, cols);
	unsigned rmid = getpos(x+1, y,   cols);
	unsigned rbot = getpos(x+1, y+1, cols);

	if (x >= cols || y >= rows) {
		return;
	}

	if (BLUE == in[pos]) {
		in[pos] = GREEN;
		go(in, parent, ltop, pos, found);
		go(in, parent, lmid, pos, found);
		go(in, parent, lbot, pos, found);
		go(in, parent, mtop, pos, found);
		go(in, parent, mbot, pos, found);
		go(in, parent, rtop, pos, found);
		go(in, parent, rmid, pos, found);
		go(in, parent, rbot, pos, found);
	} else if (BLACK == in[pos]) {
		parent[pos] = 0xFFFFFFFF;
	}
}

static unsigned
host_blink(matrix_t *m, matrix_t *p)
{
	unsigned *pixel = matrix_get_data_writeble(m);
	unsigned *parent = matrix_get_data_writeble(p);
	unsigned rows = matrix_get_rows(m);
	unsigned cols = matrix_get_cols(m);
	unsigned size = matrix_get_size(m) * sizeof(int);
	unsigned found;

	dim3 t(14, 14);
	dim3 b(1, 1);

	kernel_blink <<<b, t>>>(_in, _parent, rows, cols, _found);

	CHECK(cudaDeviceSynchronize());
	CHECK(cudaMemcpy(pixel, _in, size, cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(parent, _parent, size, cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(&found, _found, sizeof(int), cudaMemcpyDeviceToHost));

	return found;
}

static void
host_init(matrix_t *m, matrix_t *p)
{
	const unsigned *pixel = matrix_get_data_writeble(m);
	const unsigned *parent = matrix_get_data_writeble(p);
	unsigned size = matrix_get_size(m) * sizeof(int);
	unsigned found = 0xFFFFFFFF;

	CHECK(cudaSetDevice(0));
	CHECK(cudaDeviceReset());

	CHECK(cudaMalloc((void **) &_in, size));
	CHECK(cudaMalloc((void **) &_parent, size));
	CHECK(cudaMalloc((void **) &_found, sizeof(unsigned)));
	CHECK(cudaMemcpy(_in, pixel, size, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpy(_parent, parent, size, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpy(_found, &found, sizeof(unsigned), cudaMemcpyHostToDevice));
}

static void
host_end(void)
{
	CHECK(cudaFree(_in));
	CHECK(cudaFree(_parent));
	CHECK(cudaFree(_found));
}

static void
keyboard(unsigned char key, int x, int y)
{
}

static void
paint_path(matrix_t *m, const matrix_t *p, unsigned found)
{
	const unsigned *pbuf = matrix_get_data(p);
	unsigned *mbuf = matrix_get_data_writeble(m);
	unsigned pos = found;

	while (0 != pbuf[pos]) {
		mbuf[pos] = RED;
		pos = pbuf[pos];
	}

	mbuf[pos] = RED;
}

static const unsigned char *
run(void *user_data)
{
	static matrix_t *zoom = NULL;
	container_t *c = (container_t *) user_data;
	matrix_t *m = c->m;
	matrix_t *p = c->p;
	static unsigned found = 0xFFFFFFFF;

	log(".");

	/* at first call, dont call gpu */
	if (NULL == zoom) {
		zoom = matrix_zoom_in(m, _zoom_in, zoom);
		return (unsigned char *) matrix_get_data(zoom);
	}

	/* if dest is found, just show the path */
	if (0xFFFFFFFF == found) {
		found = host_blink(m, p);
	} else {
		log("found: %d\n", found);
		paint_path(m, p, found);
	}

	matrix_print_as_int(m);
	matrix_print_as_int(p);
	matrix_print(m);
	zoom = matrix_zoom_in(m, _zoom_in, zoom);
	return (unsigned char *) matrix_get_data(zoom);
}

static void
exit(void *user_data)
{
	container_t *c = (container_t *) user_data;

	host_end();
	matrix_free(c->m);
	matrix_free(c->p);

	log("vida loka!");
}

int
main(int argc, char *argv[])
{
	matrix_t *m = NULL;
	gluda_t g[1];
	container_t c[1];

	if (argc < 2) {
		log("usage: %s, %d <maze file name>", argv[0], argc);
		return -1;
	}

	if (argc == 3)
		_zoom_in = (atoi(argv[2]));

	m = parser_run(argv[1]);
	if (NULL == m) {
		log("could not load the maze");
		return -2;
	}

	matrix_print(m);
	c->m = m;
	c->p = matrix_new(matrix_get_rows(m), matrix_get_cols(m));

	g->user_data = c;
	g->keyboard_cb = keyboard;
	g->run_cb = run;
	g->exit_cb = exit;
	if (false == gluda_init(g, _zoom_in*matrix_get_rows(m), _zoom_in*matrix_get_cols(m))) {
		log("could not initialize gluda :(")
		goto end;
	}

	log("pixel: %p", matrix_get_data(m));

	host_init(m, c->p);
	gluda_run(&argc, argv);


end:
	matrix_free(m);

	printf("vida loka!\n");
}
