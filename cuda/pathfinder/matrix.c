#include "matrix.h"

#include "pixel.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct matrix
{
	pixel_t *pixel;
	unsigned rows;
	unsigned cols;
};

static char
int2char(const int i)
{
	char c = '?';

	switch(i) {
	case WHITE: c = ' '; break;
	case BLACK: c = '#'; break;
	case BLUE: c = '.'; break;
	case RED: c = '0'; break;
	}

	return c;
}

void
matrix_print(const matrix_t *m)
{
	unsigned row, col;

	for (row = 0; row < m->rows; row++) {
		for (col = 0; col < m->cols-1; col++) {
			printf("%c", int2char(m->pixel[row * m->cols + col].i));
		}
		printf("%c\n", int2char(m->pixel[row * m->cols + col].i));
	}
}

void
matrix_print_as_int(const matrix_t *m)
{
	unsigned row, col;

	for (row = 0; row < m->rows; row++) {
		for (col = 0; col < m->cols-1; col++) {
			printf("%X ", m->pixel[row * m->cols + col].i);
		}
		printf("%X\n", m->pixel[row * m->cols + col].i);
	}
}

bool
matrix_is_equal(const matrix_t *m1, const matrix_t *m2, const unsigned rows, const unsigned cols)
{
	unsigned i;

	for (i = 0; i < cols * rows; i++) {
		if (m1->pixel[i].i != m2->pixel[i].i) {
			return false;
		}
	}

	return true;
}

matrix_t *
matrix_new(const unsigned rows, const unsigned cols)
{
	matrix_t *m = (matrix_t *) malloc(sizeof(matrix_t));
	m->pixel = (pixel_t *) calloc(rows * cols, sizeof(pixel_t));
	m->rows = rows;
	m->cols = cols;

	return m;
}

void
matrix_free(matrix_t *m)
{
	if (NULL != m) {
		free(m->pixel);
	}

	free(m);
}

bool
matrix_set(matrix_t *m, const unsigned row, const unsigned col, const int data)
{
	unsigned pos;

	if (row >= m->rows || col >= m->cols) {
		log("ops -> row: %u, col: %u, m->rows: %u, m->cols: %u", row, col, m->rows, m->cols);
		return false;
	}

	pos = col + row * m->cols;
	m->pixel[pos].i = data;

	return true;
}

unsigned
matrix_get_rows(matrix_t *m)
{
	return m->rows;
}

unsigned
matrix_get_cols(matrix_t *m)
{
	return m->cols;
}

unsigned const *
matrix_get_data(const matrix_t *m)
{
	return (const unsigned *) m->pixel;
}

unsigned *
matrix_get_data_writeble(matrix_t *m)
{
	return (unsigned *) m->pixel;
}

matrix_t *
matrix_zoom_in(const matrix_t *m, unsigned fator, matrix_t *zoom)
{
	matrix_t *n = zoom;
	unsigned r, c, rf, cf, dr, dc;

	if (NULL == n)
		n = matrix_new(fator*m->rows, fator*m->cols);

	for (r = 0; r < m->rows; r++) {
		for (c = 0; c < m->cols; c++) {

			for (rf = 0; rf < fator; rf++) {
				for (cf = 0; cf < fator; cf++) {

					dr = r * fator + rf;
					dc = c * fator + cf;
					n->pixel[dr * n->cols + dc].i = m->pixel[r * m->cols + c].i;
				}
			}
		}
	}
	return n;
}

unsigned int
matrix_get_size(matrix_t *m)
{
	return m->rows * m->cols;
}

#ifdef __TEST__
int
main(void)
{
	log("vida loka!");

	return 0;
}
#endif /* __TEST__ */
