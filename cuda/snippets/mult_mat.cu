#include <stdio.h>
#include <cuda_runtime.h>

#include "common.h"

__device__ unsigned
get_pos(const unsigned row, const unsigned col, const unsigned width) {

	return GETPOS(row, col, width);
}

__global__ void
kernel(int *a, int *b, int *r)
{
	int row = threadIdx.y;
	int col = threadIdx.x;
	int width = blockDim.y;
	int pos;

	

	pos = GETPOS(row, col, width);

	r[pos] = a[pos] + b[pos];
}

#define HEIGHT 4
#define WIDTH 4
#define ARRAY_SIZE (HEIGHT * WIDTH)

int
main(void)
{
	int a[ARRAY_SIZE] = {  0,  1,  2,  3,
						   4,  5,  6,  7,
						   8,  9, 10, 11,
						  12, 13, 14, 15 };

	int b[ARRAY_SIZE] = {  1,  0,  0,  0,
					 	   0,  1,  0,  0,
						   0,  0,  1,  0,
					 	   0,  0,  0,  1 };

	int r[ARRAY_SIZE] = { 69, 69, 69, 69,
						  69, 69, 69, 69,
						  69, 69, 69, 69,
						  69, 69, 69, 69 };

	int *d_a;
	int *d_b;
	int *d_r;

	CHECK_ERROR(cudaSetDevice(0));
	CHECK_ERROR(cudaDeviceReset());

	CHECK_ERROR(cudaMalloc((void**)&d_a, ARRAY_SIZE * sizeof(int)));
	CHECK_ERROR(cudaMalloc((void**)&d_b, ARRAY_SIZE * sizeof(int)));
	CHECK_ERROR(cudaMalloc((void**)&d_r, ARRAY_SIZE * sizeof(int)));
	
	CHECK_ERROR(cudaMemcpy(d_a, a, ARRAY_SIZE * sizeof(int), cudaMemcpyHostToDevice));
	CHECK_ERROR(cudaMemcpy(d_b, b, ARRAY_SIZE * sizeof(int), cudaMemcpyHostToDevice));

	dim3 dimBlock(WIDTH, HEIGHT);
	kernel<<<1, dimBlock>>>(d_a, d_b, d_r);

	CHECK_ERROR(cudaDeviceSynchronize());

	CHECK_ERROR(cudaMemcpy(r, d_r, ARRAY_SIZE * sizeof(int), cudaMemcpyDeviceToHost));

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_r);

	print_matriz(a, WIDTH, HEIGHT);
	print_matriz(b, WIDTH, HEIGHT);
	print_matriz(r, WIDTH, HEIGHT);

	return EXIT_SUCCESS;
}
