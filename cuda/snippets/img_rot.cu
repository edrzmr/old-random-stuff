#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#define cimg_display 0
#include "CImg.h"

#include "common.h"

#define BLOCK_SIZE 16


// Define the files that are to be save and the reference images for validation
const char *imageFilename = "lena512.bmp";
const char *outputFilename = "lena512_rot.bmp";

////////////////////////////////////////////////////////////////////////////////
// Constants
const float angle = 0.5f;        // angle to rotate image by (in radians)

// Texture reference for 2D float texture
texture<float, cudaTextureType2D, cudaReadModeElementType> tex;


__global__ void transformKernel(float *outputData,
	int width,
	int height,
	float theta)
{
	// calculate normalized texture coordinates
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	float u = (float)x - (float)width / 2;
	float v = (float)y - (float)height / 2;
	float tu = u*cosf(theta) - v*sinf(theta);
	float tv = v*cosf(theta) + u*sinf(theta);

	tu /= (float)width;
	tv /= (float)height;

	// read from texture and write to global memory
	outputData[y*width + x] = tex2D(tex, tu + 0.5f, tv + 0.5f);
}


using namespace cimg_library;

int main(void) {

	cudaEvent_t e_Start,
		e_Stop;

	CImg<float> image(imageFilename);

	if (image.is_empty()){
		printf("Unable to source image file: %s\n", imageFilename);
		exit(-1);
	}

	//Reset no device
	CHECK_ERROR(cudaDeviceReset());

	//Criando eventos
	CHECK_ERROR(cudaEventCreate(&e_Start));
	CHECK_ERROR(cudaEventCreate(&e_Stop));

	unsigned int size = image.width() * image.height() * sizeof(float);
	printf("Loaded '%s', %d x %d pixels\n", imageFilename, image.width(), image.height());

	// Allocate device memory for result
	float *dData = NULL;
	CHECK_ERROR(cudaMalloc((void **)&dData, size));

	// Allocate array and copy image data
	cudaChannelFormatDesc channelDesc =
		cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
	cudaArray *cuArray;
	CHECK_ERROR(cudaMallocArray(&cuArray,
		&channelDesc,
		image.width(),
		image.height()));

	CHECK_ERROR(cudaMemcpyToArray(cuArray,
		0,
		0,
		image.data(),
		size,
		cudaMemcpyHostToDevice));

	// Set texture parameters
	tex.addressMode[0] = cudaAddressModeWrap;
	tex.addressMode[1] = cudaAddressModeWrap;
	tex.filterMode = cudaFilterModeLinear;
	tex.normalized = true;    // access with normalized texture coordinates

	// Bind the array to the texture
	CHECK_ERROR(cudaBindTextureToArray(tex, cuArray, channelDesc));

	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(image.width() / dimBlock.x, image.height() / dimBlock.y, 1);

	// Execute the kernel
	CHECK_ERROR(cudaEventRecord(e_Start, cudaEventDefault));
	transformKernel << <dimGrid, dimBlock, 0 >> >(dData, image.width(), image.height(), angle);
	CHECK_ERROR(cudaEventRecord(e_Stop, cudaEventDefault));

	float gpu_processing = 0;
	CHECK_ERROR(cudaEventSynchronize(e_Stop));
	CHECK_ERROR(cudaEventElapsedTime(&gpu_processing, e_Start, e_Stop));

	CHECK_ERROR(cudaDeviceSynchronize());

	printf("Processing time: %lf (ms)\n", gpu_processing);
	printf("%.2f Mpixels/sec\n",
		(image.width() * image.height() / (gpu_processing / 1000.0f)) / 1e6);


	// Allocate mem for the result on host side
	float *hOutputData = (float *)malloc(size);
	// copy result from device to host
	CHECK_ERROR(cudaMemcpy(hOutputData,
		dData,
		size,
		cudaMemcpyDeviceToHost));

	CImg<float> imgTransformed;
	imgTransformed.assign(hOutputData, image.width(), image.height(), 1, 1);

	imgTransformed.save(outputFilename);

	printf("Wrote '%s'\n", outputFilename);

	CHECK_ERROR(cudaFree(dData));
	CHECK_ERROR(cudaFreeArray(cuArray));

	printf("Success!\n");
}
