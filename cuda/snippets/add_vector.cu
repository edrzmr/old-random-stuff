#include <stdio.h>
#include <cuda_runtime.h>

#include "common.h"

__global__ void
kernel(int *a, int *b, int *r)
{
	r[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
}

#define SIZE 4
int
main(void)
{
	int a[SIZE] = { 9, 9, 9, 9 };
	int b[SIZE] = { 1, 1, 1, 1 };
	int r[SIZE] = { 69 , 69, 69, 69 };

	int *d_a;
	int *d_b;
	int *d_r;

	CHECK_ERROR(cudaSetDevice(0));
	CHECK_ERROR(cudaDeviceReset());

	cudaMalloc((void**)&d_a, SIZE * sizeof(int));
	cudaMalloc((void**)&d_b, SIZE * sizeof(int));
	cudaMalloc((void**)&d_r, SIZE * sizeof(int));
	
	cudaMemcpy(d_a, a, SIZE * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, SIZE * sizeof(int), cudaMemcpyHostToDevice);

	kernel <<<1, SIZE>>>(d_a, d_b, d_r);

	CHECK_ERROR(cudaDeviceSynchronize());

	cudaMemcpy(r, d_r, SIZE * sizeof(int), cudaMemcpyDeviceToHost);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_r);

	print_array(a, SIZE);
	print_array(b, SIZE);
	print_array(r, SIZE);

	return EXIT_SUCCESS;
}
