#include <stdio.h>

#include "common.h"

void
print_array(const int *array, const unsigned len)
{
	unsigned i;

	for (i = 0; i < len-1; i++) {
		printf("%i, ", array[i]);
	}
	printf("%i\n", array[i]);
}

void
print_matriz(const int *m, unsigned width, unsigned height)
{
	unsigned row, col;

	for (row = 0; row < height; row++) {

		col = 0;
		printf("[%i,%i,%i] -> ", row, col, GETPOS(row, col, height));

		for (; col < width - 1; col++) {

			printf("%i, ", m[GETPOS(row, col, height)]);
		}
		printf("%i\n", m[GETPOS(row, col, height)]);
	}
	printf("\n");
}
